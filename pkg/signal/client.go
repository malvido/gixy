// client.go : client sigCon to a sexy server

package signal

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"

	"github.com/gorilla/websocket"
)

type SexyClient struct {
	Url         string
	UserID      string
	DeviceID    string
	DeviceToken string
	sigCon      *SigCon
	stuns       []string
	turns       []string
}

// CreateSexyClient create and initialise a client sigCon
func CreateSexyClient(user, deviceID, deviceToken string) *SexyClient {
	var client SexyClient

	client.UserID = user
	client.DeviceID = deviceID
	client.DeviceToken = deviceToken
	client.sigCon = nil
	client.Url = ""

	return &client
}

func (client *SexyClient) SetServer(scheme, host, path string) error {
	if client.sigCon != nil {
		return errors.New("sigCon is open")
	}
	if scheme != "ws" && scheme != "wss" {
		return errors.New("scheme is not websocket")
	}
	url := url.URL{Scheme: scheme, Host: host, Path: path}
	client.Url = url.String()
	return nil
}

func (client *SexyClient) Connect() error {
	var err error
	if client.sigCon != nil {
		return errors.New("already connected to " + client.Url)
	}
	connection, _, err := websocket.DefaultDialer.Dial(client.Url, nil)
	if err != nil {

		return err
	}
	client.sigCon = CreateSigCon(connection)
	if client.sigCon == nil {
		return errors.New("failed to create signaling connection")
	}
	go client.sigCon.GoWriter()
	return nil
}

func (client *SexyClient) Authenticate() error {
	userhash, err := strconv.ParseUint(client.DeviceToken, 16, 32)
	if err != nil {
		return errors.New("erroneous device token")
	}
	var messageOut SignalMessage
	messageOut.MessageType = HAIL
	messageOut.UserType = JOURNEYMAN
	messageOut.UserID = client.UserID
	messageOut.UserDeviceID = client.DeviceID

	client.sigCon.Write(&messageOut)

	messageIn, err := client.sigCon.Read()
	if err != nil {
		return err
	}

	if messageIn.MessageType != CHALLENGE {
		return errors.New("wrong authentication sequence")
	}

	challenge, err := strconv.ParseUint(messageIn.Info, 16, 32)
	if err != nil || challenge == 0 {
		return errors.New("erroneous challenge")
	}
	challengeResponse := Response(uint32(challenge), uint32(userhash))
	messageOut.MessageType = RESPONSE
	messageOut.Info = fmt.Sprintf("%08x", challengeResponse)

	client.sigCon.Write(&messageOut)

	messageIn, err = client.sigCon.Read()
	if err != nil {
		return err
	}
	if messageIn.MessageType != OK {
		return errors.New("authentication failed")
	}

	return nil
}

// AddStun adds a stun server for signaling purposes
func (client *SexyClient) AddStun(stun string) {
	client.stuns = append(client.stuns, stun)
}

func (client *SexyClient) Read() (*SignalMessage, error) {
	message, err := client.sigCon.Read()
	return message, err
}

func (client *SexyClient) Close() {
	client.sigCon.Close()
	client.sigCon = nil
}
