// protocol.go : common signaling protocol

package signal

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"hash/crc32"
	"math/rand"
	"time"
)

// MessageTypeDef : types of messages that may be exchanged between signal server and peers
type MessageTypeDef int

// Valid types
const (
	OK           MessageTypeDef = 0
	ERROR                       = 1
	HAIL                        = 2
	BYE                         = 3
	RETRY                       = 4
	CHALLENGE                   = 5
	RESPONSE                    = 6
	HEART                       = 7
	BEAT                        = 8
	STUN                        = 9
	TURN                        = 10
	RING                        = 101
	PENDING                     = 102
	ACCEPT                      = 103
	DECLINE                     = 104
	CANCEL                      = 105
	CREATE                      = 106
	INVITE                      = 107
	JOIN                        = 108
	OFFER                       = 201
	ANSWER                      = 202
	HANGUP                      = 203
	ICE                         = 301
	NEWVOLUNTEER                = 401
)

// UserTypeDef types of users that can access the system
type UserTypeDef int

// Valid types
const (
	UNDETERMINED UserTypeDef = 0
	MASTER                   = 1
	JOURNEYMAN               = 2
	APPRENTICE               = 3
)

// ConnectionTypeDef types of possible WebRTC connections
type ConnectionTypeDef int

// Valid types
const (
	VIDEO ConnectionTypeDef = 0
	AUDIO                   = 1
	DATA                    = 2
)

// TurnParams parameters for accessing the TURN server
type TurnParams struct {
	Server string `json:"server"`
	Port   string `json:"port"`
	User   string `json:"user"`
	Pass   string `json:"pass"`
}

// PeerData human readable data of the peer
type PeerData struct {
	Alias     string `json:"alias"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Picture   string `json:"picture"`
}

// SignalMessage the messages exchanged with the clients
type SignalMessage struct {
	MessageType    MessageTypeDef    `json:"messagetype"`
	UserType       UserTypeDef       `json:"usertype"`
	ConnectionType ConnectionTypeDef `json:"connectiontype,omitempty"`
	Info           string            `json:"info,omitempty"`
	UserID         string            `json:"userid"`
	UserDeviceID   string            `json:"userdeviceid"`
	PeerID         string            `json:"peerid,omitempty"`
	PeerType       UserTypeDef       `json:"peertype"`
	PeerDeviceID   string            `json:"peerdeviceid,omitempty"`
	PeerData       PeerData          `json:"peerdata,omitempty"`
	MessageID      string            `json:"messageid,omitempty"`
	ConversationID string            `json:"conversationid,omitempty"`
	SDP            string            `json:"sdp,omitempty"`
	ProvideTurn    bool              `json:"provideTurn,omitempty"`
	TURN           TurnParams        `json:"turn,omitempty"`
	ICE            interface{}       `json:"ice,omitempty"`
}

var crcTable *crc32.Table

// Crc32 return the CRC value of a slice
func Crc32(slice []byte) uint32 {
	if crcTable == nil {
		crcTable = crc32.MakeTable(0xEDB88320)
	}
	return crc32.Checksum(slice, crcTable)
}

// Challenge return an unsigned 32 bit challenge
func Challenge() uint32 {
	return rand.Uint32()
}

// Response to a challenge for a particular user hash
func Response(challenge uint32, userhash uint32) uint32 {
	slice := fmt.Sprintf("%08x%08x", challenge, userhash)
	return Crc32([]byte(slice))
}

// DeadlineString get a string with the last timestamp second in which the password is valid
func DeadlineString(seconds int64) string {
	var deadline string
	lastSecond := time.Now().Unix()
	deadline = fmt.Sprintf("%d", lastSecond)
	return deadline
}

// Ephemeral TURN server password
func Ephemeral(secret string, user string) string {
	var password string

	signKey := []byte(secret)
	binPass := hmac.New(sha1.New, signKey)
	binPass.Write([]byte(user))
	fmt.Println(binPass.Sum(nil))
	password = base64.StdEncoding.EncodeToString(binPass.Sum(nil))

	return password
}

// NewTurnParams return a new TURN structure with generated secrets
func NewTurnParams(secret string, deadline int64) TurnParams {
	var turn TurnParams
	turn.User = DeadlineString(deadline)
	turn.Pass = Ephemeral(secret, turn.User)
	return turn
}
