// sigcon.go : handles safe concurrent reading and writing to a websocket

package signal

import (
	"encoding/json"
	"errors"

	"github.com/gorilla/websocket"
)

// SingleConnWriter an object to write concurrently to a websocket
type SigCon struct {
	Connection  *websocket.Conn
	ConnChannel chan *SignalMessage
}

// CreateSigCon create a single sigCon for a connection
func CreateSigCon(connection *websocket.Conn) *SigCon {
	var sigCon SigCon
	sigCon.Connection = connection
	sigCon.ConnChannel = make(chan *SignalMessage)

	return &sigCon
}

// GoWriter the concurrent go function
func (sigCon *SigCon) GoWriter() {
	for {
		message := <-sigCon.ConnChannel
		if message == nil {
			break
		}
		messageBytes, _ := json.Marshal(*message)
		sigCon.Connection.WriteMessage(websocket.TextMessage, messageBytes)
	}
	sigCon.Connection.Close()
	close(sigCon.ConnChannel)
}

// Write safely a signaling message
func (sigCon *SigCon) Write(message *SignalMessage) {
	sigCon.ConnChannel <- message
}

// Read a signaling message
func (sigCon *SigCon) Read() (*SignalMessage, error) {
	message := SignalMessage{}
	messageType, messageBytes, err := sigCon.Connection.ReadMessage()
	if err != nil {
		return nil, err
	}
	if messageType != websocket.TextMessage {
		return nil, errors.New("message type error")
	}
	err = json.Unmarshal(messageBytes, &message)
	if err != nil {
		return nil, errors.New("message parse error")
	}

	return &message, nil
}

func (sigCon *SigCon) Close() {
	sigCon.Write(nil)
}
