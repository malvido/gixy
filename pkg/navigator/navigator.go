// navigator.go :: remote file system utilities

package navigator

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
)

// FDType a file descriptor type
type FDType struct {
	Name        string
	IsDirectory bool
	Size        int64
}

// FSType a file system object
type FSType struct {
	separator   string
	BaseDir     string
	CurrentDir  string
	pageSize    int
	currentFile int
	files       []os.DirEntry
}

func findSeparator(path string) string {
	var index int
	index = strings.Index(path, "/")
	if index >= 0 {
		return path[index : index+1]
	}
	index = strings.Index(path, "\\")
	if index >= 0 {
		return path[index : index+1]
	}
	return ""
}

// SetBaseDir set the root directory for the object
func (nav *FSType) SetBaseDir(path string) error {
	var err error

	if path == "" {
		path, err = os.Getwd()
		if err != nil {
			return err
		}
	} else {
		path = filepath.Clean(path)
	}
	err = os.Chdir(path)
	if err != nil {
		return err
	}
	path, err = os.Getwd()
	if err != nil {
		return err
	}
	nav.separator = findSeparator(path)
	if nav.separator == "" {
		return errors.New("cannot determine separator character in " + path)
	}

	nav.BaseDir = path
	nav.CurrentDir = "." + nav.separator

	return nil
}

func (nav *FSType) changeDir(path string) error {
	var newCurrent string
	if path[0:1] == nav.separator {
		newCurrent = "." + path
	} else {
		newCurrent = nav.CurrentDir + path
	}

	newCurrent = filepath.Clean(newCurrent)

	ncLen := len(newCurrent)

	if ncLen > 0 && newCurrent[ncLen-1:] != nav.separator {
		newCurrent += nav.separator
	}

	dirInfo, err := os.Stat(newCurrent)
	if err != nil {
		return err
	}
	if !dirInfo.IsDir() {
		return errors.New("changeDir path is not a directory")
	}

	nav.files = nil
	nav.CurrentDir = newCurrent
	return nil
}

func (nav *FSType) ChangeDir(path string) error {
	if strings.Index(path, "..") >= 0 {
		return errors.New("ChangeDir illegal directory change")
	}
	return nav.changeDir(path)
}

func (nav *FSType) ParentDir() error {
	return nav.changeDir("..")
}

func (nav *FSType) RootDir() error {
	return nav.changeDir(nav.separator)
}

func (nav *FSType) NextFiles(howMany int) ([]FDType, error) {
	if howMany <= 0 {
		return nil, errors.New("page size must be greater than zero")
	}

	var err error

	if nav.files == nil {
		nav.files, err = os.ReadDir(nav.CurrentDir)
		nav.currentFile = 0

		if err != nil {
			nav.files = nil
			return nil, err
		}
	}
	if nav.currentFile+howMany > len(nav.files) {
		howMany = len(nav.files) - nav.currentFile
	}

	files := make([]FDType, howMany)

	for i := 0; i < howMany; i++ {
		files[i].Name = nav.files[i+nav.currentFile].Name()
		files[i].IsDirectory = nav.files[i+nav.currentFile].IsDir()
		info, err := nav.files[i+nav.currentFile].Info()
		if err == nil {
			files[i].Size = info.Size()
		} else {
			files[i].Size = -1
		}
	}

	nav.currentFile += howMany

	return files, nil
}
