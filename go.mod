module gitlab.com/malvido/gixy

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/rs/zerolog v1.22.0
	github.com/spf13/viper v1.7.1
)

replace gitlab.com/malvido/gixy/ => ./
