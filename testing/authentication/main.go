// authentication : testing an authentication sequence with sexy

package main

import (
	"fmt"

	"gitlab.com/malvido/gixy/pkg/signal"
)

func createClient() *signal.SexyClient {
	client := signal.CreateSexyClient("vahiz", "8833cc53", "5eb6f090")

	client.SetServer("wss", "anarr.es:5353", "/journeyman")
	fmt.Println("Server Url: " + client.Url)
	return client
}

func main() {
	fmt.Println("Creating client instance.")
	client := createClient()

	fmt.Println("Connecting to server.")
	err := client.Connect()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer client.Close()
	fmt.Println("Connected.")
	err = client.Authenticate()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Authentication successful.")
}
