// main.go :: test navigator package functionality
package main

import (
	"fmt"
	"os"

	"gitlab.com/malvido/gixy/pkg/navigator"
)

func main() {
	var dir navigator.FSType
	var err error

	err = dir.SetBaseDir("")
	if err != nil {
		fmt.Println(err)
		return
	}

	if len(os.Args) > 1 {
		err = dir.ChangeDir(os.Args[1])
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	files, err := dir.NextFiles(30)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Base dir " + dir.BaseDir)
	fmt.Println("Files in " + dir.CurrentDir)
	for i := 0; i < len(files); i++ {
		fmt.Println(files[i])
	}

	err = dir.ParentDir()
	if err != nil {
		fmt.Println(err)
		return
	}
	files, err = dir.NextFiles(30)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Files in " + dir.CurrentDir)
	for i := 0; i < len(files); i++ {
		fmt.Println(files[i])
	}

	err = dir.RootDir()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Files in " + dir.CurrentDir)
	for {
		files, err = dir.NextFiles(3)
		if err != nil {
			fmt.Println(err)
			return
		}
		for i := 0; i < len(files); i++ {
			fmt.Println(files[i])
		}
		if len(files) < 3 {
			break
		}
	}
}
