// gixy : (c) 2021 Jose Alvarez

package main

import (
	"fmt"
	"io"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/journald"
	"github.com/spf13/viper"
	"gitlab.com/malvido/gixy/pkg/navigator"
	"gitlab.com/malvido/gixy/pkg/signal"
)

var appName string
var logger zerolog.Logger
var dir navigator.FSType
var sigClient *signal.SexyClient

// GetLogLevel get a zerolog log level from a string
func getLogLevel(level string) zerolog.Level {
	switch level {
	case "debug":
		return zerolog.DebugLevel
	case "info":
		return zerolog.InfoLevel
	case "error":
		return zerolog.ErrorLevel
	case "fatal":
		return zerolog.FatalLevel
	case "warn":
		return zerolog.WarnLevel
	case "panic":
		return zerolog.PanicLevel
	case "trace":
		return zerolog.TraceLevel
	default:
		return zerolog.DebugLevel
	}
}

func getLogOutput(output string) io.Writer {
	switch output {
	case "journald":
		return journald.NewJournalDWriter()
	case "system":
		return journald.NewJournalDWriter()
	case "plain":
		return zerolog.ConsoleWriter{Out: os.Stdout, NoColor: true}
	case "color":
		return zerolog.ConsoleWriter{Out: os.Stdout, NoColor: false}
	default:
		return os.Stderr
	}
}

func setDefaultPrefs() {
	viper.SetDefault("log.level", "warn")
	viper.SetDefault("log.output", "color")
	viper.SetDefault("signal.host", "anarr.es:5353")
	viper.SetDefault("signal.scheme", "wss")
	viper.SetDefault("signal.path", "/journeyman")
}

func setLogPrefs() {
	zerolog.SetGlobalLevel(getLogLevel(viper.GetString("log.level")))
	logger = zerolog.New(getLogOutput(viper.GetString("log.output"))).With().Timestamp().Str("application", appName).Logger()

	logger.Info().Msgf("%s starting up", appName)
}

func setSignalingPrefs() {
	user := viper.GetString("signal.user")
	if user == "" {
		logger.Panic().Msg("missing signal.user parameter")
	}
	device := viper.GetString("signal.device")
	if device == "" {
		logger.Panic().Msg("missing signal.device parameter")
	}
	token := viper.GetString("signal.token")
	if token == "" {
		logger.Panic().Msg("missing signal.token parameter")
	}

	sigClient = signal.CreateSexyClient(user, device, token)
	sigClient.SetServer(viper.GetString("signal.scheme"), viper.GetString("signal.host"), viper.GetString("signal.path"))
	logger.Info().Msgf("signaling url: %s", sigClient.Url)
}

func initPrefs(dirName string) error {
	var err error
	appName = "gixy"

	if len(os.Args) > 1 {
		err = dir.SetBaseDir(os.Args[1])
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	viper.AddConfigPath("./")
	setDefaultPrefs()
	viper.SetConfigName(appName)
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
		return err
	}
	setLogPrefs()
	setSignalingPrefs()

	return nil
}

func handleProtocolMessage(message *signal.SignalMessage) {
	switch message.MessageType {
	case signal.STUN:
		sigClient.AddStun(message.Info)
		logger.Info().Msgf("added stun server %s", message.Info)
	default:
		logger.Warn().Msg("protocol message unhandled")
	}
}

func handleCallMessage(message *signal.SignalMessage) {
	switch message.MessageType {
	default:
		logger.Warn().Msg("call message unhandled")
	}
}

func handlePeerMessage(message *signal.SignalMessage) {
	switch message.MessageType {
	default:
		logger.Warn().Msg("peer message unhandled")
	}
}

func handleICEMessage(message *signal.SignalMessage) {
	switch message.MessageType {
	default:
		logger.Warn().Msg("ICE message unhandled")
	}
}

func main() {
	var err error
	if len(os.Args) > 1 {
		err = initPrefs(os.Args[1])
	} else {
		err = initPrefs("")
	}
	if err != nil {
		fmt.Println("initialisation failed: " + err.Error())
		return
	}

	logger.Info().Msg("connecting to signaling server")
	err = sigClient.Connect()
	if err != nil {
		logger.Fatal().Msgf("cannot connect to signaling server: %s", err.Error())
	}
	defer sigClient.Close()
	logger.Info().Msg("connected to signaling server")

	logger.Info().Msg("authenticating with signaling server")
	err = sigClient.Authenticate()
	if err != nil {
		logger.Fatal().Msgf("authentication failed: %s", err.Error())
	}
	logger.Info().Msg("authentication successful")

	for {
		message, err := sigClient.Read()
		if err != nil {
			logger.Warn().Msg(err.Error())
		} else {
			logger.Debug().Msgf("received: %v", message)
			if message.MessageType < 100 {
				handleProtocolMessage(message)
				continue
			}
			if message.MessageType < 200 {
				handleCallMessage(message)
				continue
			}
			if message.MessageType < 300 {
				handlePeerMessage(message)
				continue
			}
			if message.MessageType < 400 {
				handleICEMessage(message)
				continue
			}
			logger.Warn().Msg("message unhandled")
		}
	}

}
